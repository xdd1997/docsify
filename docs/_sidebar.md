<!-- _sidebar.md -->


* [CAD_CAE]
    * [color.md](/md_File/CAD_CAE/20220101-color.md)
    * [基于Sublime构建AnsysAPDL平台.md](/md_File/CAD_CAE/20220101-基于Sublime构建AnsysAPDL平台.md)

* [Python]
    * [基于Sublime构建AnsysAPDL平台.md](/md_File/Python/20220304-基于Sublime构建AnsysAPDL平台.md)

* [未分类]
    * [阿里云盘与夸克网盘资源搜索.md](/md_File/未分类/20230227-阿里云盘与夸克网盘资源搜索.md)
    * [Typora+PicGo+Gitee图床的几点记录.md](/md_File/未分类/20230228-Typora+PicGo+Gitee图床的几点记录.md)
    * [Git安装与使用.md](/md_File/未分类/20230301-Git安装与使用.md)
    * [docsify配置参考.md](/md_File/未分类/20230302-docsify配置参考.md)
    * [ChatGPT注册与使用.md](/md_File/未分类/20230303-ChatGPT注册与使用.md)
* [docsify+giscus.md](/md_File/20220101-docsify+giscus.md)
* [my_url.md](/md_File/20221212-my_url.md)
