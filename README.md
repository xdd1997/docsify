## 使用说明

1. 放置好md文件并修改名称后，运行 `doubleClick_me.bat`，即可自动生出侧边栏文件 `_navbar.md/README.md`
2. git上传即可 
3. github仓库: https://github.com/xdd1997/docsify
4. gitee仓库: https://gitee.com/xdd1997/docsify
3. 静态网页： [https://xdd1997.github.io/docsify](https://xdd1997.github.io/docsify)
4. 静态网页： [https://xdd1997.gitee.io/docsify](https://xdd1997.gitee.io/docsify)

   

## 注意事项

1. 文件名称必须以8个数字(YYYYMMDD)开头，并作为网页中文章时间排序方式的依据
2. 自动构建`_navbar.md`的脚本仅支持`md_File/xx/yy`三层文件目录



## 本地预览
![](https://mypic2016.oss-cn-beijing.aliyuncs.com/picGo/202303051916010.png)




## 部署

### Gitee Pages

![](https://mypic2016.oss-cn-beijing.aliyuncs.com/picGo/202211262015568.png)

### Github Pages
![](https://mypic2016.oss-cn-beijing.aliyuncs.com/picGo/202211262019706.png)
